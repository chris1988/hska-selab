package application.presentation;

import java.util.HashSet;

public class ObservableImpl implements Observable {

	private HashSet<Observer> observers = new HashSet<>(); // use hash set to find matching observers fast on detach

	public void attach(Observer observer) {
		observers.add(observer);
	}

	public void detach(Observer observer) {
		observers.remove(observer);
	}

	public void notifyObservers() {
		for (Observer observer : observers) {
			observer.update();
		}
	}

}