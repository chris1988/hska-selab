package application.presentation.javafx;

import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Callback;
import application.logic.Game;
import application.logic.GameMachine;
import application.presentation.Controller;
import application.presentation.QuizViewController;

public class GameFactory extends Application {

	@Override
	public void start(Stage primaryStage) throws IOException {
		final Game model = new GameMachine();
		final Controller controller = new QuizViewController(model);

		URL location = getClass().getResource("Quiz.fxml");
		FXMLLoader loader = new FXMLLoader(location);

		// loader.getNamespace().put("controller", controller); <?language
		// javascript?> in Fxml to delegate
		loader.setControllerFactory(new Callback<Class<?>, Object>() {

			@Override
			public Object call(Class<?> cls) {
				if (cls == QuizView.class) {
					QuizView view = new QuizView(model, controller);
					controller.setView(view);
					return view;
				} else
					try {
						return cls.newInstance();
					} catch (Exception e) {
						e.printStackTrace();
						throw new RuntimeException(e);
					}
			}

		});

		Parent root = (Parent) loader.load();
		Scene scene = new Scene(root, 800, 600);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}