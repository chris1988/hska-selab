/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package application.presentation.javafx;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import application.logic.Field;
import application.logic.Game;
import application.logic.Meeple;
import application.logic.Player;
import application.presentation.Controller;
import application.presentation.View;
import application.presentation.events.DiceEvent;
import application.presentation.events.InitGameEvent;
import application.presentation.events.SelectHomeMeepleEvent;
import application.presentation.events.SelectMeepleEvent;

/**
 * FXML Controller class
 * 
 * @author Chris
 */
public class QuizView extends View implements Initializable {

	public QuizView(Game game, Controller controller) {
		super(game, controller);
	}

	private final ObservableList<String> playerNames = FXCollections
			.observableArrayList();
	@FXML
	private Pane init_view;
	@FXML
	private Pane game_view;
	@FXML
	private Button start_button;
	@FXML
	private Button add_player_button;
	@FXML
	private TextField name_input;
	@FXML
	private ListView<String> players_list;
	@FXML
	private ImageView dice_image;
	@FXML
	private Pane player_top_right;
	@FXML
	private Pane player_top_left;
	@FXML
	private Pane player_bottom_right;
	@FXML
	private Pane player_bottom_left;
	@FXML
	private HBox home_field_top_left;
	@FXML
	private HBox home_field_bottom_left;
	@FXML
	private HBox home_field_bottom_right;
	@FXML
	private HBox home_field_top_right;
	@FXML
	private Label diced_number_label;
	@FXML
	private Label game_status_label;
	@FXML
	private Label player_top_right_name_label;
	@FXML
	private Label player_bottom_right_name_label;
	@FXML
	private Label player_bottom_left_name_label;
	@FXML
	private Label player_top_left_name_label;

	@FXML
	// Felder von 1 - 48
	private Pane game_field;

	private FadeTransition fadeTransition;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		init_view.setVisible(true);
		game_view.setVisible(false);
		players_list.setItems(playerNames);
		update();

	}

	@FXML
	private void inputChanged(KeyEvent event) {
		if (event.getCharacter().equals("\r")) {
			if (!add_player_button.isDisabled()) {
				controller.handleEvent(new InitGameEvent(InitGameEvent.ADD_PLAYER));
			}
		}
		controller.handleEvent(new InitGameEvent(InitGameEvent.INPUT_CHANGED));
	}

	@FXML
	private void startGameClicked(MouseEvent event) {
		controller.handleEvent(new InitGameEvent(InitGameEvent.START_GAME));
	}

	@FXML
	private void newGameClicked(ActionEvent event) {
		controller.handleEvent(new InitGameEvent(InitGameEvent.NEW_GAME));
	}

	@FXML
	private void addPlayerClicked(MouseEvent event) {
		controller.handleEvent(new InitGameEvent(InitGameEvent.ADD_PLAYER));
	}

	@FXML
	private void meepleClicked(MouseEvent event) {

		Circle circle = (Circle) event.getSource();
		Pane parent = (Pane) circle.getParent();
		int meepleIndex = parent.getChildrenUnmodifiable().indexOf(circle);
		// System.out.println("View Meeple Index: " + meepleIndex);
		controller.handleEvent(new SelectMeepleEvent(game.getActivePlayer(), meepleIndex));

	}

	@FXML
	private void homeMeepleClicked(MouseEvent event) {

		Circle circle = (Circle) event.getSource();
		HBox parent = (HBox) circle.getParent();
		int index = parent.getChildrenUnmodifiable().indexOf(circle);
		controller.handleEvent(new SelectHomeMeepleEvent(game.getActivePlayer(), index));
	}

	@FXML
	private void diceClicked() {
		controller.handleEvent(new DiceEvent(game.getActivePlayer()));
		// Some fancy Animation
		animateDice();

	}

	private void animateDice() {
		final ScaleTransition st = new ScaleTransition(Duration.millis(500), dice_image);
		st.setByX(1.5f);
		st.setByY(1.5f);
		st.setCycleCount(2);
		st.setAutoReverse(true);
		final RotateTransition rt = new RotateTransition(Duration.millis(500), dice_image);
		rt.setByAngle(360);
		rt.setCycleCount(2);
		final FadeTransition ft = new FadeTransition(Duration.millis(1000), dice_image);
		ft.setFromValue(1.0);
		ft.setToValue(0.0);
		ft.setCycleCount(1);
		final ParallelTransition pt = new ParallelTransition(rt, st);
		pt.play();
		pt.setOnFinished(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				pt.jumpTo(Duration.ZERO);
				dice_image.setVisible(false);
				pt.stop();
				pt.setNode(null);
				update();
			}
		});
	}

	@Override
    /**
     * main update routine for the view. this is called by the model / and controller
     * if an update is required as the state changed
     */
	public void update() {
        /* check if the game is in init state (adding players)
         * or if the game is already running (show playground)
         */
		if (game.isGameActive()) {
			// Game Running
			game_view.setVisible(true);
			init_view.setVisible(false);
			renderMeeples();
			renderHomeMeeples();
			renderDice();
			highlightActivePlayer();
			game_status_label.setText(game.getStatusMessage());
			renderPlayerNames();

		} else {
			// Init
			game_view.setVisible(false);
			init_view.setVisible(true);

            // show current player list (update it from model data)
            playerNames.clear();
            for(Player player : game.getPlayers()) {
                playerNames.add(player.getName());
            }
            // only activate start game button of game model allows it
			start_button.setDisable(!game.minPlayersReached());
		}

	}

    /**
     * renders all the player names
     */
	private void renderPlayerNames() {
		List<Player> players = game.getPlayers();
		player_top_left_name_label.setText(players.get(0).getName());
		player_top_right_name_label.setText(players.get(1).getName());
		if (players.size() > 2)
			player_bottom_left_name_label.setText(players.get(2).getName());
		if (players.size() > 3)
			player_bottom_right_name_label.setText(players.get(3).getName());
	}

    /**
     * check how many home meeples each player has and render them in the view
     */
	private void renderHomeMeeples() {
		List<Player> players = game.getPlayers();

        // get all the view elements for the home meeples
		List<HBox> templist = new LinkedList<HBox>();
		templist.add(home_field_top_left);
		templist.add(home_field_top_right);
		templist.add(home_field_bottom_left);
		templist.add(home_field_bottom_right);

        // iterate all home areas
		for (int player_number = 0; player_number < templist.size(); player_number++) {
			ObservableList<Node> circles = templist.get(player_number).getChildren();
            // iterate each home field circle
			for (int circle_number = 0; circle_number < circles.size(); circle_number++) {
				Circle circle = (Circle) circles.get(circle_number);
                // TODO this is bad style
				try {
					players.get(player_number).getHomeMeeples().get(circle_number);
					circle.setFill(players.get(player_number).getColor());
				} catch (Exception e) {
					circle.setFill(Color.WHITE);
				}
			}
		}

	}

	private void highlightActivePlayer() {
		Player player = game.getActivePlayer();
		if (fadeTransition != null) {
			fadeTransition.setToValue(1.0);
			fadeTransition.stop();
		}
		fadeTransition = new FadeTransition();
		fadeTransition.setFromValue(1.0);
		fadeTransition.setToValue(0.1);
		fadeTransition.setDuration(Duration.millis(500));
		fadeTransition.setAutoReverse(true);
		fadeTransition.setCycleCount(Timeline.INDEFINITE);

		switch (game.getIndexOfPlayer(player)) {
		case 0:
			fadeTransition.setNode(player_top_left);
			break;
		case 1:
			fadeTransition.setNode(player_top_right);
			break;
		case 2:
			fadeTransition.setNode(player_bottom_left);
			break;
		case 3:
			fadeTransition.setNode(player_bottom_right);
			break;
		default:
			return;
		}
		fadeTransition.play();

	}

	private void renderDice() {
		dice_image.setVisible(game.isDiceEnabled());
		dice_image.setRotate(0);
		dice_image.setScaleX(1);
		dice_image.setScaleY(1);
		if (fadeTransition != null) {
			fadeTransition.jumpTo(Duration.ZERO);
		}

	}

	private void renderMeeples() {
		Field curr = game.getPlayground().startField;
		for (int i = 0; i < game_field.getChildren().size(); i++) {
			Color color = Color.WHITE;
			if (!curr.getMeeples().isEmpty()) {
				int num = 0;
				for (Meeple meeple : curr.getMeeples()) {
					num++;
					// System.out.println("Meeple " + num + " on Field " + i
					// + " with color " + meeple.getColor().getRed() + " "
					// + meeple.getColor().getGreen() + " "
					// + meeple.getColor().getBlue());
					if (num > 1) {
						color = color.interpolate(meeple.getColor(), 0.5);
					} else
						color = meeple.getColor();

				}
			}
			Circle circle;
			circle = (Circle) game_field.getChildren().get(i);
			circle.setFill(color);
			curr = curr.next;

		}

	}

	public ObservableList<String> getPlayerNames() {
		return playerNames;
	}

	public Pane getInit_view() {
		return init_view;
	}

	public Pane getGame_view() {
		return game_view;
	}

	public Button getStart_button() {
		return start_button;
	}

	public Button getAdd_player_button() {
		return add_player_button;
	}

	public TextField getName_input() {
		return name_input;
	}

	public ListView<String> getPlayers_list() {
		return players_list;
	}

	public Pane getGame_field() {
		return game_field;
	}

	public Pane getPlayer_top_right() {
		return player_top_right;
	}

	public Pane getPlayer_top_left() {
		return player_top_left;
	}

	public Pane getPlayer_bottom_right() {
		return player_bottom_right;
	}

	public Pane getPlayer_bottom_left() {
		return player_bottom_left;
	}

	public HBox getHome_field_top_left() {
		return home_field_top_left;
	}

	public HBox getHome_field_bottom_left() {
		return home_field_bottom_left;
	}

	public HBox getHome_field_bottom_right() {
		return home_field_bottom_right;
	}

	public HBox getHome_field_top_right() {
		return home_field_top_right;
	}

	public void showDiceNumber(int dicedNumber) {
		diced_number_label.setText("" + dicedNumber);

	}

}
