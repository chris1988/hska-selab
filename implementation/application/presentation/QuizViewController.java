package application.presentation;

import java.util.Iterator;
import java.util.Set;

import application.logic.Field;
import application.logic.Game;
import application.logic.Meeple;
import application.presentation.events.DiceEvent;
import application.presentation.events.GameEvent;
import application.presentation.events.InitGameEvent;
import application.presentation.events.SelectHomeMeepleEvent;
import application.presentation.events.SelectMeepleEvent;
import application.presentation.javafx.QuizView;

/**
 * Created by stefan on 05.06.14.
 */
public class QuizViewController extends Controller {

	public QuizViewController(Game game) {
		super(game);
	}

	@Override
	public void update() {

	}

	@Override
	public boolean handleEvent(GameEvent event) {
		QuizView quizView = (QuizView) view;

        // INIT GAME EVENT
		if (event instanceof InitGameEvent) {
			InitGameEvent initEvent = (InitGameEvent) event;
			switch (initEvent.getType()) {
                case InitGameEvent.ADD_PLAYER:
                    game.addPlayer(quizView.getName_input().getText());
                    break;
                case InitGameEvent.INPUT_CHANGED:
                    if (quizView.getName_input().getText() == null || quizView.getName_input().getText().isEmpty()) {
                        quizView.getAdd_player_button().setDisable(true);
                    } else
                        quizView.getAdd_player_button().setDisable(false);
                    break;
                case InitGameEvent.START_GAME:
                    game.startGame();
                    break;
                case InitGameEvent.NEW_GAME:
                    game.newGame();
                    break;
                default:
                    return false;
                }



        // DICE EVENT
		} else if (event instanceof DiceEvent) {
			game.dice();
			quizView.showDiceNumber(game.getDicedNumber());



        // SELECT MEEPLE EVENT
		} else if (event instanceof SelectMeepleEvent) {
			SelectMeepleEvent meepleEvent = (SelectMeepleEvent) event;
			Field field = game.getPlayground().getField( meepleEvent.getMeepleIndex());
			Set<Meeple> meeples = field.getMeeples();
			Iterator<Meeple> it = meeples.iterator();
			while (it.hasNext()) {
				if (meepleEvent.getPlayer() == game.getActivePlayer()) {
					game.selectMeeple(it.next());
				} else it.next();
			}



        // SELECT HOME MEEPLE
		} else if (event instanceof SelectHomeMeepleEvent) {

			SelectHomeMeepleEvent meepleEvent = (SelectHomeMeepleEvent) event;
			try {
				Meeple meeple = game.getActivePlayer().getHomeMeeples().get(meepleEvent.getMeepleIndex());
				game.selectMeeple(meeple);
			} catch (Exception e) {
				// System.out.println("Kein Meeple hier "
				// + meepleEvent.getMeepleIndex());
			}

		}
		return false;
	}
}
