package application.presentation;

import application.logic.Game;
import application.presentation.events.GameEvent;

public abstract class Controller implements Observer {

	Game game; // to call actions on the logic layer
	View view; // to call manipulate()

	public void setView(View view) {
		this.view = view;
	}

	public Controller(Game game) {
		this.game = game;
		this.game.attach(this);
	}

	@Override
	public abstract void update();

	public abstract boolean handleEvent(GameEvent event);
}