package application.presentation.events;

import application.logic.Player;

public class InitGameEvent implements GameEvent {

	public static final int ADD_PLAYER = 0;
	public static final int INPUT_CHANGED = 1;
	public static final int START_GAME = 2;
	public static final int NEW_GAME = 3;

	private int type;

	public InitGameEvent(int type) {
		if (type < ADD_PLAYER || type > NEW_GAME) {
			throw new RuntimeException("Illeagal GameEvent");
		}
		this.type = type;
	}

	public int getType() {
		return type;
	}

	@Override
	public Player getPlayer() {
		return null;
	}

}
