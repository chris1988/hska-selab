package application.presentation.events;

import application.logic.Player;

public class SelectMeepleEvent implements GameEvent {

	private Player player;
	private int meepleIndex;

	public SelectMeepleEvent(Player player, int meepleIndex) {
		this.player = player;
		this.meepleIndex = meepleIndex;
	}

	@Override
	public Player getPlayer() {

		return player;
	}

	public int getMeepleIndex() {

		return meepleIndex;
	}

}
