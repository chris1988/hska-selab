package application.presentation.events;

import application.logic.Player;

public interface GameEvent {
	public Player getPlayer();

}
