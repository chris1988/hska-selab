package application.presentation.events;

import application.logic.Player;

public class SelectHomeMeepleEvent implements GameEvent {

	private Player player;
	private int meepleIndex;

	public SelectHomeMeepleEvent(Player player, int meepleIndex) {
		this.player = player;
		this.meepleIndex = meepleIndex;

	}

	@Override
	public Player getPlayer() {

		return player;
	}

	public int getMeepleIndex() {

		return meepleIndex;
	}

}
