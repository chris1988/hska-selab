package application.presentation.events;

import application.logic.Player;

public class DiceEvent implements GameEvent {

	private Player player;

	public DiceEvent(Player player) {
		this.player = player;
	}

	@Override
	public Player getPlayer() {
		return player;
	}

}
