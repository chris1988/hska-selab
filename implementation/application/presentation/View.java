package application.presentation;

import application.logic.Game;

public abstract class View implements Observer {

	protected Game game; // to read status on update call
	protected Controller controller; // to pass actions to the controller

	public View(Game game, Controller controller) {
		this.game = game;
		this.controller = controller;
		this.game.attach(this);
	}

}