package application.presentation;

public interface Observer {
	public void update();
}