package application.logic;

import java.util.HashSet;
import java.util.Set;

/**
 * single playground field
 * can handle multiple meeples
 */
public class Field {

    // contains all meeples that are currently placed on this field
	private Set<Meeple> meeples;

	// references as the playground is made of fields as a linked list
	public Field next = null;
	public Field previous = null;

	public Field() {
		meeples = new HashSet<Meeple>();
	}

    /**
     * get all meeples on this field
     * @return all meeples on this field
     */
	public Set<Meeple> getMeeples() {
		return meeples;
	}

    /**
     * place a meeple on this field
     * @param meeple the meeple to place
     */
	public void placeMeeple(Meeple meeple) {
		meeples.add(meeple);
	}

    /**
     * remove a meeple from this field
     * @param meeple the meeple to remove
     */
	public void removeMeeple(Meeple meeple) {
		meeples.remove(meeple);
	}

}
