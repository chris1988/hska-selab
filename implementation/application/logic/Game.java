package application.logic;

import java.util.List;

import application.presentation.ObservableImpl;

/*
 This is the API to the logic application layer
 */

/**
 * GAME API
 */
public abstract class Game extends ObservableImpl {

	/**
	 * player dices
	 */
	public abstract void dice();

	/**
	 * add new player to the game.
	 * only possible before the game was started
	 * @param name name of the new player to add
	 */
	public abstract void addPlayer(String name);

	/**
	 * player selects meeple after he diced.
	 * @param meeple the meeple which the player selected
	 */
	public abstract void selectMeeple(Meeple meeple);

	/**
	 * start the game after adding all players
	 */
	public abstract void startGame();

	/**
	 * reset the game and create a new game
	 */
	public abstract void newGame();


	/*
		GETTERS FOR THE VIEW
	 */

    /**
     * check if minimal number of players for a game are reached
     * @return true if minimal count of players added to the game, so the game can be started
     */
	public abstract boolean minPlayersReached();

    /**
     * check if the game is active or if players can still be added to the game
     * @return true if the game is already running
     */
	public abstract boolean isGameActive();

    /**
     * get the currently active player
     * @return the active player
     */
	public abstract Player getActivePlayer();

    /**
     * get the current playground status
     * @return the playground
     */
	public abstract Playground getPlayground();

    /**
     * check if the dice is enabled (the current player can dice)
     * @return true if the dice is enabled
     */
	public abstract boolean isDiceEnabled();

    /**
     * get the index of the player
     * @param player
     * @return the index of the player given in the array of the game machine
     */
	public abstract int getIndexOfPlayer(Player player);

    /**
     * get the number of the last dice
     * @return number of the last dice
     */
	public abstract int getDicedNumber();

    /**
     * get all players in the game
     * @return all players
     */
	public abstract List<Player> getPlayers();

    /**
     * current game status message (which state, which player's turn).
     * use this to show the player what's going on.
     * @return current game status message (which state, which player's turn)
     */
	public abstract String getStatusMessage();

}