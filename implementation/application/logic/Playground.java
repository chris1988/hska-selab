package application.logic;

/**
 * Playground. Circle list of Fields. Double linked list.
 */
public class Playground {

	public static final int FIELD_COUNT = 48;
	public final Field startField;

	public Playground() {
		Field currentField = new Field();
		startField = currentField;
		// create all the fields and link them
		// set i = 1 as the start field was created already
		for (int i = 1; i < FIELD_COUNT; i++) {
            // created doubled linked list
			Field newField = new Field();
			newField.previous = currentField;
			currentField.next = newField;
			currentField = newField;
		}
		// make circle connection
		Field lastField = currentField;
		lastField.next = startField;
		startField.previous = lastField;
	}

	/**
	 * returns the reference to the start field of the player
	 *
	 * @param thisPlayersNumber
	 *            start field for which player? starting from 0 for first player
	 * @return reference to the start field of the player
	 */
	public Field getStartField(int thisPlayersNumber) {
		int fieldNum;
		switch (thisPlayersNumber) {
            case 0:
                fieldNum = 42;
                break;
            case 1:
                fieldNum = 6;
                break;
            case 3:
                fieldNum = 18;
                break;
            case 2:
                fieldNum = 30;
                break;
            default:
                throw new RuntimeException("invalid Player");
		}
		return getField(fieldNum);
	}

    /**
     * get field by index
     * @param index
     * @return field at this index
     */
	public Field getField(int index) {
        if(index < FIELD_COUNT) {
            Field curr = startField;
            for (int i = 0; i < index; i++) {
                curr = curr.next;
            }
            return curr;
        }
        throw new IllegalArgumentException("index out of playground range");
	}

}
