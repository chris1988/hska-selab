package application.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javafx.scene.paint.Color;

public class Player {

	private static final int MEEPLE_COUNT = 3;

	private Color color;
	private String name;
	private Stack<Meeple> meeplesHome = new Stack<Meeple>();

	public Stack<Meeple> getHomeMeeples() {
		return meeplesHome;
	}

	private List<Meeple> meeples = new ArrayList<Meeple>();
	private Field startField;

	public Player(String name, Color color, Field startField) {
		this.name = name;
		this.color = color;
		this.startField = startField;

		// create new meeplesHome for the player
		for (int i = 0; i < MEEPLE_COUNT; i++) {
			Meeple meeple = new Meeple(this);
			meeplesHome.push(meeple);
			meeples.add(meeple);
		}
	}

	public void meepleHome(Meeple meeple) {
		meeplesHome.push(meeple);
	}

	public void popMeepleFromHome(Meeple meeple) {
		meeplesHome.remove(meeple);
	}

	public Color getColor() {
		return color;
	}

	public String getName() {
		return name;
	}

	public Field getStartField() {
		return startField;
	}

	public boolean hasMeepleOut() {
		return meeplesHome.size() < MEEPLE_COUNT;
	}

	public boolean hasMeepleHome() {
		return meeplesHome.size() > 0;
	}
}
