package application.logic.state;

import application.logic.GameMachine;
import application.logic.Meeple;

/**
 * abstract state class
 * contains all methods the game interface also has. this is required because the game implementation delegates work to the state methods
 */
public abstract class State {

	protected GameMachine game;

	public State(GameMachine game) {
		this.game = game;
	}

	public abstract void dice();

	public abstract void addPlayer(String name);

	public abstract void selectMeeple(Meeple meeple);

	public abstract void startGame();

	public abstract void newGame();

	public abstract String getStatusMessage();
}
