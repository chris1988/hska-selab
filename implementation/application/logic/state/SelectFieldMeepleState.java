package application.logic.state;

import application.logic.GameMachine;
import application.logic.Meeple;

/**
 * Created by stefan on 30.05.14.
 */
public class SelectFieldMeepleState extends State {
	public SelectFieldMeepleState(GameMachine gameMachine) {
		super(gameMachine);
	}

	@Override
	public void dice() {

	}

	@Override
	public void addPlayer(String name) {

	}

	@Override
	public void selectMeeple(Meeple meeple) {
		// check if the selection is valid
		if (meeple.getPlayer().equals(game.currentPlayer)
				&& meeple.isFieldMeeple()) {
			meeple.moveBy(game.currentPlayerDiced);
			game.nextPlayer();
			game.currentState = game.dice;
			game.diceEnabled = true;
		}
	}

	@Override
	public void startGame() {

	}

	@Override
	public void newGame() {
		game.reset();
		game.currentState = game.addPlayers;
	}

	@Override
	public String getStatusMessage() {
		return "Select one of your meeples on the field to move it by "
				+ game.currentPlayerDiced;
	}
}
