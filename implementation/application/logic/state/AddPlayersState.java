package application.logic.state;

import javafx.scene.paint.Color;
import application.logic.GameMachine;
import application.logic.Meeple;
import application.logic.Player;

/**
 * Created by stefan on 30.05.14.
 */
public class AddPlayersState extends State {

	public AddPlayersState(GameMachine gameMachine) {
		super(gameMachine);
	}

	@Override
	public void dice() {

	}

	@Override
	public void addPlayer(String name) {
        // check if may players reached
		if (game.players.size() < game.MAX_PLAYERS) {

            // find player's color
			Color color = null;
            switch (game.players.size()) {
                case 0:
                    color = color.BLUE;
                    break;
                case 1:
                    color = color.RED;
                    break;
                case 2:
                    color = color.GREEN;
                    break;
                case 3:
                    color = color.YELLOW;
                    break;
                default:
                    throw new IllegalArgumentException("too many players");
            }

            // add the new player
			Player newPlayer = new Player(name, color, game.playground.getStartField(game.players.size()));
			game.players.add(newPlayer);
		}
	}

	@Override
	public void selectMeeple(Meeple meeple) {

	}

	@Override
	public void startGame() {
		if (game.players.size() > 1) {
			game.currentPlayer = game.players.get(0); // first player is the
														// first to dice in
														// selectStartPlayerState
			game.currentState = game.selectStartPlayer;
			game.diceEnabled = true;
		}
	}

	@Override
	public void newGame() {
		game.reset();
		game.currentState = game.addPlayers;
	}

	@Override
	public String getStatusMessage() {
		return "Add Players. Max 4 Players. Min 2 Players.";
	}
}
