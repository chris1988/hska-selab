package application.logic.state;

import application.logic.GameMachine;
import application.logic.Meeple;

/**
 * Created by stefan on 30.05.14.
 */
public class DiceState extends State {
	public DiceState(GameMachine gameMachine) {
		super(gameMachine);
	}

	private int diceCount = 0;

	@Override
	public void dice() {
		game.generateDice();
		if (game.currentPlayerDiced != 6 && game.currentPlayer.hasMeepleOut()) {
			// select field meeple
			game.currentState = game.selectFieldMeeple;
			game.diceEnabled = false;
			diceCount = 0;
		} else if (game.currentPlayerDiced == 6
				&& game.currentPlayer.hasMeepleHome()) {
			// select home meeple
			game.currentState = game.selectHomeMeeple;
			game.diceEnabled = false;
			diceCount = 0;
		} else if (game.currentPlayerDiced == 6
				&& !game.currentPlayer.hasMeepleHome()) {
			// select field meeple
			game.currentState = game.selectFieldMeeple;
			game.diceEnabled = false;
			diceCount = 0;
		} else if (game.currentPlayerDiced != 6
				&& !game.currentPlayer.hasMeepleOut()) {
			if (diceCount < 2) {
				// dice again
				diceCount++;
			} else {
				diceCount = 0;
				game.nextPlayer();
			}
		}
	}

	@Override
	public void addPlayer(String name) {

	}

	@Override
	public void selectMeeple(Meeple meeple) {

	}

	@Override
	public void startGame() {

	}

	@Override
	public void newGame() {
		game.reset();
		game.currentState = game.addPlayers;
	}

	@Override
	public String getStatusMessage() {
		return "Please click on the dice.";
	}
}
