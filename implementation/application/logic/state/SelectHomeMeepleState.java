package application.logic.state;

import application.logic.GameMachine;
import application.logic.Meeple;

/**
 * Created by stefan on 30.05.14.
 */
public class SelectHomeMeepleState extends State {
	public SelectHomeMeepleState(GameMachine gameMachine) {
		super(gameMachine);
	}

	@Override
	public void dice() {

	}

	@Override
	public void addPlayer(String name) {

	}

	@Override
	public void selectMeeple(Meeple meeple) {
		// check if the selection is valid
		if (meeple.getPlayer().equals(game.currentPlayer)
				&& meeple.isHomeMeeple()) {
			meeple.moveOut();
			game.nextPlayer();
			game.currentState = game.dice;
			game.diceEnabled = true;
		}
	}

	@Override
	public void startGame() {

	}

	@Override
	public void newGame() {
		game.reset();
		game.currentState = game.addPlayers;
	}

	@Override
	public String getStatusMessage() {
		return "You diced " + game.currentPlayerDiced
				+ ". You can now move one of your home meeples out.";
	}
}
