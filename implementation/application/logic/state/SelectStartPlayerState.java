package application.logic.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import application.logic.GameMachine;
import application.logic.Meeple;
import application.logic.Player;

/**
 * Created by stefan on 30.05.14.
 */
public class SelectStartPlayerState extends State {

	public SelectStartPlayerState(GameMachine gameMachine) {
		super(gameMachine);
	}

	// store which values each player diced
	private Map<Player, Integer> diceStore = new HashMap<Player, Integer>();
	// manage current player also internally because if > 1 player has max value
	// there has to be a battle between the remaining players
	public List<Player> remainingPlayers = null;
	private Player currentPlayer;

	@Override
	public void dice() {

		// init remaining players when arriving at this point for the first time
		if (remainingPlayers == null) {
			remainingPlayers = new ArrayList<Player>(game.players); // TODO:
																	// does this
																	// keep the
																	// order?
			// fetch current player from the game class
			currentPlayer = game.currentPlayer;
		}

		game.generateDice();
		diceStore.put(currentPlayer, game.currentPlayerDiced); // store it

		// check if all remaining players have diced
		if (allRemainingDiced()) {
			// check if one player has max
			Player winner = findWinner();
			if (winner != null) {
				game.currentState = game.dice;
				game.currentPlayer = winner;
			} else {
				// else battle between remaining players, so remove the losers
				// from the battle
				removeLosers();

				// at least two players remaining!
				assert remainingPlayers.size() > 1;

				currentPlayer = remainingPlayers.get(0); // TODO: this probably
															// breaks order of
															// players
				game.currentPlayer = currentPlayer;
			}
		} else {
			nextPlayer();
		}
	}

	private void removeLosers() {
		int max = findMaxValue();

		// collect all the losers
		List<Player> losers = new ArrayList<Player>();
		for (Map.Entry<Player, Integer> entry : diceStore.entrySet()) {
			if (entry.getValue().compareTo(max) < 0) {
				// this player did dice less than the max value
				losers.add(entry.getKey());
			}
		}

		diceStore.clear();
		remainingPlayers.removeAll(losers);
	}

	/**
	 * find max diced value in the dice store
	 * 
	 * @return
	 */
	private int findMaxValue() {
		int max = 0;
		for (Integer diced : diceStore.values()) {
			if (diced > max)
				max = diced;
		}
		return max;
	}

	private void nextPlayer() {
		int currentPlayerNumber = remainingPlayers.indexOf(currentPlayer);
		currentPlayerNumber++;
		currentPlayerNumber %= remainingPlayers.size();
		currentPlayer = remainingPlayers.get(currentPlayerNumber);

		game.currentPlayer = currentPlayer; // also update the game
	}

	/**
	 * check if all remaining players have diced
	 * 
	 * @return
	 */
	private boolean allRemainingDiced() {
		return diceStore.size() == remainingPlayers.size();
	}

	/**
	 * find the winner of the start player selection
	 * 
	 * @return the winner. if not clear, return null
	 */
	private Player findWinner() {
		int max = findMaxValue();

		// check how often this value is in the store
		int playersWithMax = 0;
		Player player = null;
		for (Map.Entry<Player, Integer> entry : diceStore.entrySet()) {
			if (entry.getValue().equals(max)) {
				playersWithMax++;
				player = entry.getKey();
			}
		}

		// if there is only one player with this max value, he is the winner
		if (playersWithMax == 1) {
			return player;
		} else
			return null;
	}

	@Override
	public void addPlayer(String name) {

	}

	@Override
	public void selectMeeple(Meeple meeple) {

	}

	@Override
	public void startGame() {

	}

	@Override
	public void newGame() {
		game.reset();
		game.currentState = game.addPlayers;
	}

	@Override
	public String getStatusMessage() {
		return "Selecting start player. Player with highest dice count wins.";
	}
}
