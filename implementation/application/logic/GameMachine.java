package application.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import application.logic.state.AddPlayersState;
import application.logic.state.DiceState;
import application.logic.state.SelectFieldMeepleState;
import application.logic.state.SelectHomeMeepleState;
import application.logic.state.SelectStartPlayerState;
import application.logic.state.State;

/**
 * Implementation of the Game API Interface
 */
public class GameMachine extends Game {

	public static int MAX_PLAYERS = 4;

	// need to be public as the State handle() methods change the currentState
	// of the state machine
	public State currentState;
	public State addPlayers;
	public State selectStartPlayer;
	public State dice;
	public State selectHomeMeeple;
	public State selectFieldMeeple;

    // list of all the players of the current game
	public List<Player> players;
    // current player
	public Player currentPlayer;
    // last diced value
	public int currentPlayerDiced;
    // game's playground
	public Playground playground;
    // is it allowed to dice right now?
	public boolean diceEnabled;

	public GameMachine() {
		reset();
		currentState = addPlayers;
	}

    /**
     * prepare everything for a new game
     */
	public void reset() {
		addPlayers = new AddPlayersState(this);
		selectStartPlayer = new SelectStartPlayerState(this);
		dice = new DiceState(this);
		selectHomeMeeple = new SelectHomeMeepleState(this);
		selectFieldMeeple = new SelectFieldMeepleState(this);

		diceEnabled = false;
		players = new ArrayList<Player>();
		playground = new Playground();
	}

    /**
     * switch current player to the next player in the list
     * switch to first player if last player was reached
     */
	public void nextPlayer() {
		int currentPlayerNumber = players.indexOf(currentPlayer);
		currentPlayerNumber++;
		currentPlayerNumber %= players.size();
		currentPlayer = players.get(currentPlayerNumber);
	}

	public void generateDice() {
		Random random = new Random();
		currentPlayerDiced = random.nextInt(6) + 1; // random returns 0-5, so
													// add 1
	}


    /*
     * API METHODS
     * delegate to the current state methods
     */

	@Override
	public void dice() {
		currentState.dice();
		notifyObservers();
	}

	@Override
	public void addPlayer(String name) {
		currentState.addPlayer(name);
		notifyObservers();
	}

	@Override
	public void selectMeeple(Meeple meeple) {
		currentState.selectMeeple(meeple);
		notifyObservers();
	}

	@Override
	public void startGame() {
		currentState.startGame();
		notifyObservers();
	}

	@Override
	public void newGame() {
		currentState.newGame();
		notifyObservers();
	}

	@Override
	public boolean minPlayersReached() {
		return players.size() > 1;
	}

	@Override
	public boolean isGameActive() {
		if (currentState instanceof AddPlayersState) {
			return false;
		} else
			return true;
	}

	@Override
	public Player getActivePlayer() {
		return currentPlayer;
	}

	@Override
	public Playground getPlayground() {
		return playground;
	}

	@Override
	public boolean isDiceEnabled() {
		return diceEnabled;
	}

	@Override
	public int getIndexOfPlayer(Player player) {
		return players.indexOf(player);
	}

	@Override
	public int getDicedNumber() {
		return currentPlayerDiced;
	}

	@Override
	public List<Player> getPlayers() {
		return players;
	}

	@Override
	public String getStatusMessage() {
		if (currentState == selectStartPlayer) {
			return currentState.getStatusMessage();
		}
		return currentPlayer.getName() + "'s turn. "
				+ currentState.getStatusMessage();
	}

}
