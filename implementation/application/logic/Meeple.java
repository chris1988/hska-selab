package application.logic;

import javafx.scene.paint.Color;

/**
 * A single meeple on the playground
 */
public class Meeple {

	private Player player; // each meeple belongs to a player
	private Field currentField = null; // each meeple knows on which field he currently stands (except if he is in the home area of a player)

	public Meeple(Player player) {
		this.player = player;
	}

    /**
     * get the color of the meeple. It is the player of the player it belongs to
     * @return
     */
	public Color getColor() {
		return player.getColor();
	}

    /**
     * get the player this meeple belongs to
     * @return
     */
	public Player getPlayer() {
		return player;
	}

    /**
     * move this meeple out from to home field to the playground start field of the player it belongs to
     */
	public void moveOut() {
		player.popMeepleFromHome(this);
		moveToField(player.getStartField());
	}

    /**
     * move this meeple back to the start field of the player
     */
	public void moveBackToStart() {
		moveToField(player.getStartField());
	}

    /**
     * move this meeple back into the home field of the player
     */
	public void moveHome() {
		player.meepleHome(this);
		if (currentField != null) {
			currentField.removeMeeple(this);
			currentField = null;
		}
	}

	/**
	 * moves the meeple on the playground by n steps. n can also be negative to move backwards.
	 * 
	 * @param steps number of steps to move
	 */
	public void moveBy(int steps) {
        // check if the meeple is on the playground yet
		if (currentField != null) {
			Field newField = currentField;
            // handle forward or backward steps
			if (steps > 0) {
				for (int i = 0; i < steps; i++) {
					newField = newField.next;
				}
				moveToField(newField);
			} else {
				steps *= -1;
				for (int i = 0; i < steps; i++) {
					newField = newField.previous;
				}
				moveToField(newField);
			}
		}
	}

    /**
     * move this meeple to the given playground field
     * @param field
     */
	private void moveToField(Field field) {
        // remove from current field
		if (currentField != null)
			currentField.removeMeeple(this);
        // place on the new field
		field.placeMeeple(this);
        // remember current field
		this.currentField = field;
	}

    /**
     * check if this meeple is currently in the home area of the player
     * @return true if it is in the home area
     */
	public boolean isHomeMeeple() {
		return currentField == null;
	}
	public boolean isFieldMeeple() {
		return !isHomeMeeple();
	}
}
