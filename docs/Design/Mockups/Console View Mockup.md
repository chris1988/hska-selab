#Console View Mockup

Die Konsolenausgabe teilt sich in folgende drei Teile:

```
SPIELZUSTAND

SPIELFELD

MÖGLICHE AKTIONEN
```

Vor Start des Spiels:

```
Herzlich Willkommen zum Lernquiz.

Zum Starten eines neues Spiels "ENTER" drücken.
```

Danach werden die Spieler eingetragen:

```
Spieler eingeben.

Bereits vorhandene Spieler: Markus, Peter

Zum Abbrechen des aktuellen Spiels "ESC" drücken.
Um einen weiteren Spieler hinzuzufügen, Namen eingeben und "ENTER" drücken.
Keine weiteren Spieler mehr? Zum Spiel starten "START" eingeben und "ENTER" drücken.
```

Danach wird ausgewürfelt, wer beginnen darf.

```
Startspieler auswürfeln.
Markus ist am Zug.

Peter hat gewürfelt: 4
Lisa hat noch nicht gewürfelt.

Zum Abbrechen des aktuellen Spiels "ESC" drücken.
Zum Würfeln "x" drücken.
```


Während des Spiels sieht das so aus:

```
Markus ist am Zug.

Markus: 0/3 Wissensstreitern im Spiel
Peter:  2/3 Wissensstreitern im Spiel: Peter1 Feld 23, Peter2 Feld 10
Lisa:   1/3 Wissensstreitern im Spiel: Lisa1 Feld 42

Zum Abbrechen des aktuellen Spiels "ESC" drücken.
Zum Würfeln "x" drücken.
```

Nach dem Würfeln werden dann die Wissensstreiter bewegt:

```
Markus ist am Zug.
Markus hat eine 5 gewürfelt.

Markus: 0/3 Wissensstreitern im Spiel
Peter:  2/3 Wissensstreitern im Spiel: Peter1 Feld 23, Peter2 Feld 10
Lisa:   1/3 Wissensstreitern im Spiel: Lisa1 Feld 42

Zum Abbrechen des aktuellen Spiels "ESC" drücken.
Zum Würfeln "x" drücken.
```

```
Peter ist am Zug.
Peter hat eine 3 gewürfelt.

Markus: 0/3 Wissensstreitern im Spiel
Peter:  2/3 Wissensstreitern im Spiel: Peter1 Feld 23, Peter2 Feld 10
Lisa:   1/3 Wissensstreitern im Spiel: Lisa1 Feld 42

Zum Abbrechen des aktuellen Spiels "ESC" drücken.
Um den Wissensstreiter auzuwählen, der bewegt werden soll, dessen Name eingeben und dann "ENTER" drücken. Beispiel: "Peter1"
```