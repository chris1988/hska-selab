import java.util.Random;


public class Model {
	
		private Random rand = new Random();
		private int gewuerfelteZahl;
		private int zaehler = 0;
		

		public int getGewuerfelteZahl() {
			return gewuerfelteZahl;
		}


		public int wuerfeln (){
			this.gewuerfelteZahl = rand.nextInt(6)+1;
			if (gewuerfelteZahl == 6){
				return State.State_Ziehen;
			}
			else return State.State_Wuerfeln;
		} 

		public int ziehen() {
			zaehler = zaehler+1;
			if (zaehler == 3){
				return State.State_Fertig;
			}
			else return State.State_Wuerfeln;
		}

		public void fertig() {
			System.exit(0);
		}
		

}
