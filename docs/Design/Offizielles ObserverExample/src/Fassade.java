
public class Fassade extends Subject implements IModel  {
	Model sModel;
	State currentState;
	
	
	public Fassade(){
		this.sModel = new Model();
		this.currentState = new State();
	}


	public void wuerfeln() {
		if (this.currentState.getState()== State.State_Wuerfeln){
			System.out.println("Fassade SysOp wuerfeln");	// Kontrollausgabe
			this.currentState.setState(sModel.wuerfeln());
			notifyObservers(this.currentState.getState());
		}
	}


	public void ziehen() {
		if (this.currentState.getState()== State.State_Ziehen){
			System.out.println("Fassade SysOp ziehen");	// Kontrollausgabe
			this.currentState.setState(sModel.ziehen());		
			notifyObservers(this.currentState.getState());
		}
	}

	public void fertig() {
		if (this.currentState.getState()== State.State_Fertig){
			System.out.println("Fassade SysOp fertig");	// Kontrollausgabe
			sModel.fertig();		
		}
	}



	public int getGewuerfelteZahl() {
		return sModel.getGewuerfelteZahl();
	}
	

}
