public class State {
	
	public static final int State_Wuerfeln = 1;
	public static final int State_Ziehen = 2;
	public static final int State_Fertig = 3;
	

	
	private int state = State_Wuerfeln; 
	
	
	public int getState() {   
		return state;
	} 
	
	public void setState(int state) {
		this.state = state;
	}
	

}
